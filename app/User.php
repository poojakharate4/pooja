<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    const REGISTERED_VIA_WEB = 1;
    const REGISTERED_VIA_ADMIN = 2;
    const REGISTERED_VIA_FACEBOOK = 3;
    const REGISTERED_VIA_GOOGLE = 4;

    const REGISTERED_VIA = [
        self::REGISTERED_VIA_WEB => 'Web',
        self::REGISTERED_VIA_ADMIN => 'Admin',
        self::REGISTERED_VIA_FACEBOOK => 'Facebook',
        self::REGISTERED_VIA_GOOGLE => 'Google',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();


        if(is_null($check)){
            return static::create($input);
        }


        return $check;
    }
}

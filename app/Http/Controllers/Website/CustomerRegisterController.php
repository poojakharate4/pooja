<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Mail\NewUserMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class CustomerRegisterController extends Controller
{
    public function create()
    {
        return view('user.customer.register');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:8|max:10',
        ]);

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'address' => $request->get('address'),
            'password' => Hash::make($request->get('password')),
            'registered_via' => User::REGISTERED_VIA_WEB
        ]);
        $user->assignRole('customer');

        $details = [
            'email' => $user->email
        ];

        Mail::to($user->email)->send(new NewUserMail($details));
        Auth::login($user);

        return redirect()->route('website.dashboard')->with(['success' => 'Registered successfully!!']);
    }
}

<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Services\SocialFacebookAccountService;

class FacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }
    /**
     * Return a callback method from facebook api.
     *
     * @return \Illuminate\Http\RedirectResponse URL from facebook
     */
    public function callback(User $service)
    {
        try {
            $user = Socialite::driver('facebook')->user();
            dd($user);
            $create['name'] = $user->getName();
            $create['email'] = $user->getEmail();
            $create['facebook_id'] = $user->getId();


            $userModel = new User;
            $createdUser = $userModel->addNew($create);
            Auth::loginUsingId($createdUser->id);


            return redirect()->route('website.dashboard');


        } catch (Exception $e) {


            return redirect('redirect');


        }
    }
}

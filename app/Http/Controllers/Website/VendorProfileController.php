<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class VendorProfileController extends Controller
{
    public function create()
    {
        return view('user.vendor.profile', ['user' => Auth::user()]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'store_address' => 'required',
            'store_name' => 'required',
            'store_description' => 'required',
            'contact_number' => 'required',
            'profile_image' => 'required',
            'banner_image' => 'required',
            ]);

        if (Auth::user()) {
            if($request->hasFile('profile_image')){
                $profile_image = $request->file('profile_image');
                $profile_filename = time() . '.' . $profile_image->getClientOriginalExtension();

                Image::make($profile_image)->resize(300, 300)->save( public_path('uploads/' . $profile_filename ) );
            };

            if($request->hasFile('banner_image')){
                $image = $request->file('banner_image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(300, 300)->save( public_path('uploads/' . $filename ) );
            };
            $user = Auth::user();
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->address = $request->get('store_address');
            $user->store_name = $request->get('store_name');
            $user->store_description = $request->get('store_description');
            $user->contact_number = $request->get('contact_number');
            $user->save();

            return redirect()->back()->with(['success' => 'Profile updated successfully!!']);
        }

        return redirect()->back()->with(['error' => 'Something went wrong'])->withInput();
    }
}

<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function create()
    {
        return view('user.customer.change-password');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:8|max:8',
            'password_confirmation' =>'required'
        ]);

        if (Auth::user()) {
            $user = Auth::user();
            $user->password = Hash::make($request->get('password'));
            $user->save();

            return redirect()->back()->with(['success' => 'Password updated successfully!!']);
        }

        return redirect()->back()->with(['error' => 'Something went wrong'])->withInput();
    }
}

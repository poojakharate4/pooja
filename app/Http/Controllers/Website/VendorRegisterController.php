<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Mail\NewUserMail;
use App\Mail\SendUserMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class VendorRegisterController extends Controller
{
    public function create()
    {
        return view('user.vendor.register');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'store_name' => 'required',
            'store_address' => 'required',
            'store_description' => 'required',
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:8|max:10',
            'profile_image' => 'required',
            'banner_image' => 'required',
        ]);

//        $profile = $request->file('profile_image');
//        $extension = $profile->getClientOriginalExtension();
//        Storage::disk('public')->put($profile->getFilename().'.'.$extension,  File::get($profile));
//
//        $banner = $request->file('banner_image');
//        $extension = $banner->getClientOriginalExtension();
//        Storage::disk('public')->put($banner->getFilename().'.'.$extension,  File::get($banner));

        if($request->hasFile('profile_image')){
            $profile_image = $request->file('profile_image');
            $profile_filename = time() . '.' . $profile_image->getClientOriginalExtension();

            Image::make($profile_image)->resize(300, 300)->save( public_path('uploads/' . $profile_filename ) );
        };

        if($request->hasFile('banner_image')){
            $image = $request->file('banner_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save( public_path('uploads/' . $filename ) );
        };

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'address' => $request->get('store_address'),
            'store_name' => $request->get('store_name'),
            'store_description' => $request->get('store_description'),
            'contact_number' => $request->get('contact_number'),
            'password' => Hash::make($request->get('password')),
            'profile_image' => $profile_filename,
            'banner_image' => $filename,
            'registered_via' => User::REGISTERED_VIA_WEB
        ]);

        $user->assignRole('vendor');

        $details = [
            'email' => $user->email
        ];

        Mail::to($user->email)->send(new NewUserMail($details));
        Auth::login($user);

        return redirect()->route('website.dashboard')->with(['success' => 'Registered successfully!!']);
    }
}

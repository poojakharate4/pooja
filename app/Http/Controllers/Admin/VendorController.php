<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\SendUserMail;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Spatie\QueryBuilder\QueryBuilder;
use Yajra\DataTables\Facades\DataTables;

class VendorController extends Controller
{
    public function index(Request $request)
    {
//        if ($request->ajax()) {
//            $userQuery = QueryBuilder::for(User::class)
//                ->defaultSort('id');
//
//            if ($request->ajax()) {
//                return DataTables::of($userQuery)
//                    ->editColumn('created_at', function (User $user) {
//                        return $user->created_at->format('d:m:Y');
//                    })
//                    ->editColumn('name', function (User $user) {
//                        return $user->first_name.' '.$user->last_name;
//                    })
//                    ->addColumn('action', 'admin.vendor.dataTable.action')
//                    ->rawColumns(['action'])
//                    ->addIndexColumn()
//                    ->toJson();
//            }
//        }

        $vendors = User::whereHas('roles', function ($q) {
            $q->where('name', 'vendor');
        })->get();

        return view('admin.vendor.index', ['vendors' => $vendors]);
    }

    public function create()
    {
        return view('admin.vendor.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'store_name' => 'required',
            'store_address' => 'required',
            'store_description' => 'required',
            'email' => 'required|email:rfc,dns',
            'profile_image' => 'required',
            'banner_image' => 'required',
        ]);

        $randomPwd = mt_rand(11111111, 99999999);
        $hashPwd = Hash::make($randomPwd);

        try {
            DB::transaction(function () use ($request, $randomPwd, $hashPwd) {
                if ($request->hasFile('profile_image')) {
                    $profile_image = $request->file('profile_image');
                    $profile_filename = time() . '.' . $profile_image->getClientOriginalExtension();

                    Image::make($profile_image)->resize(300, 300)->save(public_path('uploads/' . $profile_filename));
                };

                if ($request->hasFile('banner_image')) {
                    $image = $request->file('banner_image');
                    $filename = time() . '.' . $image->getClientOriginalExtension();
                    Image::make($image)->resize(300, 300)->save(public_path('uploads/' . $filename));
                };

                $user = User::create([
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'email' => $request->get('email'),
                    'address' => $request->get('store_address'),
                    'store_name' => $request->get('store_name'),
                    'store_description' => $request->get('store_description'),
                    'contact_number' => $request->get('contact_number'),
                    'password' => $hashPwd,
                    'profile_image' => $profile_filename,
                    'banner_image' => $filename,
                    'registered_via' => User::REGISTERED_VIA_ADMIN
                ]);

                $user->assignRole('vendor');

                $details = [
                    'email' => $user->email,
                    'password' => $randomPwd
                ];

                Mail::to($user->email)->send(new SendUserMail($details));

            });

            return redirect()->route('admin.vendor.index')->with(['success' => 'Customer created successfully!!']);


        } catch (Throwable $e) {
            return redirect()->back()->with(['error' => 'Something went wrong. Please try again.'])->withInput();
        }
    }

    public function edit(User $user)
    {
        return view('admin.vendor.edit', ['vendor' => $user]);
    }

    public
    function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'store_name' => 'required',
            'store_address' => 'required',
            'store_description' => 'required',
            'email' => 'required|email:rfc,dns',
            'profile_image' => 'required',
            'banner_image' => 'required',
        ]);

        $randomPwd = mt_rand(11111111, 99999999);
        $hashPwd = Hash::make($randomPwd);

        $profile_filename = null;
        $filename = null;

        if ($request->hasFile('profile_image')) {
            $profile_image = $request->file('profile_image');
            $profile_filename = time() . '.' . $profile_image->getClientOriginalExtension();

            Image::make($profile_image)->resize(300, 300)->save(public_path('uploads/' . $profile_filename));
        };

        if ($request->hasFile('banner_image')) {
            $image = $request->file('banner_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300, 300)->save(public_path('uploads/' . $filename));
        };

        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'address' => $request->get('store_address'),
            'store_name' => $request->get('store_name'),
            'store_description' => $request->get('store_description'),
            'contact_number' => $request->get('contact_number'),
            'password' => $hashPwd,
            'profile_image' => $profile_filename,
            'banner_image' => $filename,
        ]);

        return redirect()->route('admin.vendor.index')->with(['success' => 'Customer updated successfully!!']);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public
    function delete(User $user)
    {
        try {
            $user->delete();
        } catch (Exception $e) {
            return redirect()->back()->with(['error' => 'Something went wrong. Please try again.']);
        }

        return redirect()->route('admin.vendor.index')->with(['success' => 'User deleted successfully.']);
    }
}

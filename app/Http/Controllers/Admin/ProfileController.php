<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function create()
    {
        return view('admin.profile', ['user' => Auth::user()]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            ]);

        if (Auth::user()) {
            $user = Auth::user();
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->save();

            return redirect()->back()->with(['success' => 'Profile updated successfully!!']);
        }

        return redirect()->back()->with(['error' => 'Something went wrong'])->withInput();
    }
}

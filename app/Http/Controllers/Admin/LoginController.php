<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function create()
    {
        return view('admin.auth.login');
    }

    public function store(Request  $request)
    {
        if ($user = User::whereEmail($request->get('email'))->first()) {
            if (Hash::check($request->get('password'), $user->password)) {

                if ($user->roles->first()->name != 'admin')
                {
                    return redirect()->back()->with(['error' => 'Your are not authorized to login']);
                }
                else
                {
                    Auth::login($user);
                    return redirect()->route('admin.dashboard');
                }

            }

        }

        return redirect()->back()->with(['error' => 'Invalid E-mail or Password']);
    }

    public function destroy()
    {
        Auth::logout();

        return redirect()->route('admin.auth.login');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\SendUserMail;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Spatie\QueryBuilder\QueryBuilder;
use Throwable;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
//        if ($request->ajax()) {
//            $userQuery = QueryBuilder::for(User::class)
//                ->defaultSort('id');
//
//            if ($request->ajax()) {
//                return DataTables::of($userQuery)
//                    ->editColumn('created_at', function (User $user) {
//                        return $user->created_at->format('d:m:Y');
//                    })
//                    ->editColumn('name', function (User $user) {
//                        return $user->first_name.' '.$user->last_name;
//                    })
//                    ->addColumn('action', 'admin.customer.dataTable.action')
//                    ->rawColumns(['action'])
//                    ->addIndexColumn()
//                    ->toJson();
//            }
//        }

        $customers = User::whereHas('roles', function ($q) {
            $q->where('name', 'customer');
        })->get();

        return view('admin.customer.index', ['customers' => $customers]);
    }

    public function create()
    {
        return view('admin.customer.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'email' => 'required|email:rfc,dns',
        ]);

        $randomPwd = mt_rand(11111111, 99999999);
        $hashPwd = Hash::make($randomPwd);
//        try {
            DB::transaction(function () use ($request, $randomPwd, $hashPwd) {
                $user = User::create([
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'email' => $request->get('email'),
                    'address' => $request->get('address'),
                    'password' => Hash::make($hashPwd),
                    'registered_via' => User::REGISTERED_VIA_ADMIN
                ]);
                $user->assignRole('customer');

                $details = [
                    'email' => $user->email,
                    'password' => $randomPwd
                ];

                Mail::to($user->email)->send(new SendUserMail($details));
            });

            return redirect()->route('admin.customer.index')->with(['success' => 'Customer created successfully!!']);


//        }
//        catch (Throwable $e) {
//            return redirect()->back()->with(['error' => 'Something went wrong. Please try again.'])->withInput();
//        }
    }

    public function edit(User $user)
    {
        return view('admin.customer.edit', ['customer' => $user]);
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'email' => 'required|email:rfc,dns',
        ]);

        $randomPwd = mt_rand(11111111, 99999999);
        $hashPwd = Hash::make($randomPwd);

        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'address' => $request->get('address'),
            'password' => Hash::make($hashPwd),
        ]);

        return redirect()->route('admin.customer.index')->with(['success' => 'Customer updated successfully!!']);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function delete(User $user)
    {
        try {
            $user->delete();
        } catch (Exception $e) {
            return redirect()->back()->with(['error' => 'Something went wrong. Please try again.']);
        }

        return redirect()->route('admin.customer.index')->with(['success' => 'User deleted successfully.']);
    }
}

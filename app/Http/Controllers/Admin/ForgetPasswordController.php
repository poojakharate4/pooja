<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\AdminSendForgotPasswordSMS;
use App\Mail\ForgetPasswordMail;
use App\Mail\NewUserMail;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class ForgetPasswordController extends Controller
{
    public function create()
    {
        return view('admin.forget-password');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function Store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email:rfc,dns|exists:users,email',
//            'password' => 'required|confirmed',
//            'password_confirmation' =>'required'
        ]);

        if ($user = User::role('admin')->whereEmail($request->get('email'))->first()) {
            if ($user->roles()->where('name', 'admin')->first()) {
                $randomPwd = mt_rand(11111111, 99999999);
                $hashPwd = Hash::make($randomPwd);
                $user->update(['password' => Hash::make($hashPwd)]);

                $details = [
                    'password' => $randomPwd
                ];

                Mail::to($user->email)->send(new ForgetPasswordMail($details));

                return redirect()
                    ->route('admin.auth.login')
                    ->with(['success' => 'Password Changed Successfully.']);
            } else {
                return redirect()->back()->with('error', 'The Email is invalid.');
            }
        } else {
            return redirect()->back()->with('error', 'Email is invalid.');
        }
    }
}

@extends('admin.layouts.master')

@section('title', 'Customers')

@section('content')
    <div class="container" style="margin-left: 200px;">
        <form action="{{ route('admin.customer.store') }}"
              method="post">
            @csrf
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="bg-soft-primary">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="text-primary pl-3 mt-3">
                                            <p>Create Customer</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-4">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">First Name</label>
                                            <input class="form-control form-control-sm" type="text" name="first_name"
                                                   value="{{old('first_name')}}" placeholder="Enter your first name" required>
                                            @foreach($errors->get('first_name') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Last Name</label>
                                            <input class="form-control form-control-sm" type="text" name="last_name"
                                                   value="{{old('last_name')}}"  placeholder="Enter your last name" required>
                                            @foreach($errors->get('last_name') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Email</label>
                                            <input class="form-control form-control-sm" type="email" name="email"
                                                   value="{{old('email')}}" placeholder="Enter your email" required>
                                            @foreach($errors->get('email') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Address</label>
                                            <input class="form-control form-control-sm" type="text" name="address"
                                                   value="{{old('address')}}"  placeholder="Enter your address" required>
                                            @foreach($errors->get('address') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="btn text-center">
                                            <button class="btn btn-success font-weight-bold" type="submit">
                                                Create
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

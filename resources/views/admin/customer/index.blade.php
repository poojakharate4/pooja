@extends('admin.layouts.master')

@section('title', 'Customers')

@section('content')
    <div class="container">
        <div class="row" style="margin-left: 200px">
            <div class="col-lg-12 mx-auto">
                <div class="container">
                        <div class="btn-group" role="group" style="margin-left: 500px">
                            <a href="{{ route('admin.customer.create') }}"
                               class="btn btn-primary waves-effect waves-light font-weight-bold text-white">
                                <i class="mdi mdi-plus"></i> Create
                            </a>
                        </div>
                </div>
                <h4 class="header-title"> Customers</h4>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Created at</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Address</td>
                        <td>Registered Via</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $key => $customer)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $customer->created_at->format('d-m-Y') }}</td>
                            <td>{{ $customer->first_name }} {{ $customer->last_name }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->address }}</td>
                            <td>
                                @if( $customer->registered_via == 1) Web
                                @elseif($customer->registered_via == 2) Admin
                                @elseif($customer->registered_via == 3) Facebook
                                @else
                                    Google
                                @endif
                            </td>
                            <td> <a href="{{ route('admin.customer.delete',['user' => $customer]) }}"
                                    class="btn btn-primary waves-effect waves-light font-weight-bold text-white">
                                    <i class="mdi mdi-plus"></i> Delete
                                </a>
                                <a href="{{ route('admin.customer.edit',['user' => $customer]) }}"
                                   class="btn btn-primary waves-effect waves-light font-weight-bold text-white">
                                    <i class="mdi mdi-plus"></i> Edit
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

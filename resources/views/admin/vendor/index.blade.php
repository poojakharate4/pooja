@extends('admin.layouts.master')

@section('title', 'Vendors')

@section('content')
    <div class="container">
        <div class="row" style="margin-left: 200px">
            <div class="col-lg-12 mx-auto">
                <div class="container">
                    <div class="btn-group" role="group" style="margin-left: 500px">
                        <a href="{{ route('admin.vendor.create') }}"
                           class="btn btn-primary waves-effect waves-light font-weight-bold text-white">
                            <i class="mdi mdi-plus"></i> Create
                        </a>
                    </div>
                </div>
                <h4 class="header-title"> Vendors</h4>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Date</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Store Name</td>
                        <td>Registered Via</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vendors as $key => $vendor)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $vendor->created_at->format('d-m-Y') }}</td>
                            <td>{{ $vendor->first_name }} {{ $vendor->last_name }}</td>
                            <td>{{ $vendor->email }}</td>
                            <td>{{ $vendor->store_name }}</td>
                            <td>@if( $vendor->registered_via == 1) Web
                                @elseif($vendor->registered_via == 2) Admin
                                @elseif($vendor->registered_via == 3) Facebook
                                @else
                                    Google
                                @endif
                            </td>
                            <td><a href="{{ route('admin.vendor.edit',['user' => $vendor]) }}"
                                   class="btn btn-primary waves-effect waves-light font-weight-bold text-white">
                                    <i class="mdi mdi-plus"></i> Edit
                                </a>
                                <a href="{{ route('admin.vendor.delete',['user' => $vendor]) }}"
                                   class="btn btn-primary waves-effect waves-light font-weight-bold text-white">
                                    <i class="mdi mdi-plus"></i> Delete
                                </a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

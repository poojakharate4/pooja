@extends('admin.layouts.master')

@section('title', 'Vendors')

@section('content')
    <div class="container" style="margin-left: 200px;">
        <form action="{{ route('admin.vendor.update', ['user' => $vendor]) }}"
              method="post">
            @csrf
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="bg-soft-primary">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="text-primary pl-3 mt-3">
                                            <p>Edit Vendor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-4">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">First Name</label>
                                            <input class="form-control form-control-sm" type="text" name="first_name"
                                                   value="{{old('first_name', $vendor->first_name)}}" placeholder="Enter your first name" required>
                                            @foreach($errors->get('first_name') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Last Name</label>
                                            <input class="form-control form-control-sm" type="text" name="last_name"
                                                   value="{{old('last_name', $vendor->last_name)}}"  placeholder="Enter your last name" required>
                                            @foreach($errors->get('last_name') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Email</label>
                                            <input class="form-control form-control-sm" type="email" name="email"
                                                   value="{{old('email', $vendor->email)}}" placeholder="Enter your email" required>
                                            @foreach($errors->get('email') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Store Name</label>
                                            <input class="form-control form-control-sm" type="text" name="store_name"
                                                   value="{{ old('store_name', $vendor->store_name) }}" placeholder="Enter your Store Name" required>
                                            @foreach($errors->get('store_name') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Store Address</label>
                                            <input class="form-control form-control-sm" type="text" name="store_address"
                                                   value="{{ old('store_address', $vendor->address) }}" placeholder="Enter your Store Address" required>
                                            @foreach($errors->get('store_address') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Store Description</label>
                                            <input class="form-control form-control-sm" type="text" name="store_description"
                                                   value="{{ old('store_description', $vendor->store_description) }}"  placeholder="Enter your Store Description" required>
                                            @foreach($errors->get('store_description') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Contact Number</label>
                                            <input class="form-control form-control-sm" type="text" name="contact_number"
                                                   value="{{ old('contact_number', $vendor->contact_number) }}"  placeholder="Enter your Contact Number" required>
                                            @foreach($errors->get('contact_number') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Banner Image</label>
                                            <input type="file" name="banner_image"
                                                   required accept="image/*">
                                            <img style="width:100px;height:auto;" src="{{asset(public_path('uploads/' .$vendor->banner_image))}}"/>
                                            @foreach($errors->get('banner_image') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-4">
                                            <label class="required">Profile Image</label>
                                            <input type="file" name="profile_image"
                                                   required accept="image/*">
                                            <img style="width:100px;height:auto;" src="{{asset(public_path('uploads/' .$vendor->profile_image))}}"/>
                                            @foreach($errors->get('profile_image') as $error)
                                                <div class="text-danger font-weight-bold">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="btn text-center">
                                            <button class="btn btn-success font-weight-bold" type="submit">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

<nav class="navbar navbar-expand-sm bg-light" style="margin-left: 800px">
    <ul class="navbar-nav">
        <li class="nav-item">

        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Setting
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <a class="nav-link" href="{{ route('admin.profile') }}">Edit Profile</a>
                <a class="nav-link" href="{{ route('admin.change-password') }}">Change Password</a>
                <a class="nav-link" href="{{ route('admin.login.destroy') }}">Logout</a>
            </ul>
        </li>
    </ul>
</nav>

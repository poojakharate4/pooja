<nav class="navbar bg-light">
    <ul class="navbar-nav">
        <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.customer.index') }}"> Customers</a>
                <a class="nav-link" href="{{ route('admin.vendor.index') }}"> Vendors</a>
        </li>
    </ul>
</nav>

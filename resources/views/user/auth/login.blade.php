<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Pooja</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta content="admin" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
</head>

<body class="overflow-hidden">
<div class="container">
    <form action="{{ route('website.login.store') }}"
          method="post">
        @csrf
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-8">
                                <div class="text-primary pl-3 mt-3">
                                    <p>Sign in to Website.</p>
                                </div>
                            </div>
                            <div class="container">
                                <div class="btn text-center justify-content-center">
                                    <a class="btn btn-primary font-weight-bold" href="{{ route('website.redirect') }}">
                                       Login with Facebook
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="form-group mb-4">
                                <label class="required">Email Address</label>
                                <input class="form-control form-control-sm" type="email" name="email"
                                       placeholder="Enter your email" required>
                                @foreach($errors->get('email') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>


                            <div class="form-group mb-4">
                                <label class="required">Password</label>
                                <input class="form-control form-control-sm" type="password" name="password"
                                       placeholder="Enter your password" required>
                                @foreach($errors->get('password') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>
                            <div class="container">
                                <div class="btn text-center justify-content-center">
                                    <button class="btn btn-success font-weight-bold" type="submit">
                                        Login
                                    </button>
                                </div>

                                <a href="{{ route('website.customer.register') }}" class="mr-4">
                                    Customer Registration
                                </a>
                                <a href="{{ route('website.vendor.register') }}">
                                    Vendor Registration
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

@if(Session::has('success'))
    <script>
        Swal.fire('Yay!!!', '{{ Session::get('success') }}', 'success')
    </script>
@endif

@if(Session::has('error'))
    <script>
        Swal.fire('Oops!!!', '{{ Session::get('error') }}', 'error')
    </script>
@endif
</body>
</html>

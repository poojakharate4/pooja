@extends('user.layouts.master')

@section('title', 'Dashboard')

@section('content')
    <div class="container" style="margin-left: 200px;">
        <form action="{{ route('website.profile.store') }}"
              method="post">
            @csrf
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="form-group mb-4">
                                <label class="required">First Name</label>
                                <input class="form-control form-control-sm" type="text" name="first_name"
                                       value="{{ old('first_name', $user->first_name) }}"
                                       placeholder="Enter your First Name" required>
                                @foreach($errors->get('first_name') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>

                            <div class="form-group mb-4">
                                <label class="required">Last Name</label>
                                <input class="form-control form-control-sm" type="text" name="last_name"
                                       value="{{ old('last_name', $user->last_name) }}"
                                       placeholder="Enter your Last Name" required>
                                @foreach($errors->get('last_name') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>


                            <div class="form-group mb-4">
                                <label class="required">Email</label>
                                <input class="form-control form-control-sm" type="email" name="email"
                                       value="{{ old('email', $user->email) }}" placeholder="Enter your email" required
                                       disabled>
                                @foreach($errors->get('email') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>

                            <div class="form-group mb-4">
                                <label class="required">Address</label>
                                <input class="form-control form-control-sm" type="text" name="address"
                                       value="{{ old('address', $user->address) }}"
                                       placeholder="Enter your Address" required>
                                @foreach($errors->get('address') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>

                            <div class="btn text-center justify-content-center">
                                <button class="btn btn-success font-weight-bold" type="submit">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

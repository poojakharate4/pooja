<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Customer Registration</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta content="admin" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body class="overflow-hidden">
<div class="container">
    <form action="{{ route('website.customer.register.store') }}"
          method="post">
        @csrf
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="bg-soft-primary">
                            <div class="row">
                                <div class="col-8">
                                    <div class="text-primary pl-3 mt-3">
                                        <p>Website Login</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-4">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">First Name</label>
                                        <input class="form-control form-control-sm" type="text" name="first_name"
                                              value="{{old('first_name')}}" placeholder="Enter your first name" required>
                                        @foreach($errors->get('first_name') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Last Name</label>
                                        <input class="form-control form-control-sm" type="text" name="last_name"
                                               value="{{old('last_name')}}"  placeholder="Enter your last name" required>
                                        @foreach($errors->get('last_name') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Email</label>
                                        <input class="form-control form-control-sm" type="email" name="email"
                                               value="{{old('email')}}" placeholder="Enter your email" required>
                                        @foreach($errors->get('email') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Address</label>
                                        <input class="form-control form-control-sm" type="text" name="address"
                                               value="{{old('address')}}"  placeholder="Enter your address" required>
                                        @foreach($errors->get('address') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Password</label>
                                        <input class="form-control form-control-sm" type="password" name="password"
                                               placeholder="Enter your password" required>
                                        @foreach($errors->get('password') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Confirm Password</label>
                                        <input class="form-control form-control-sm" type="password" name="password_confirmation"
                                               placeholder="Enter your password_confirmation" required>
                                        @foreach($errors->get('password_confirmation') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="btn text-center">
                                        <button class="btn btn-success font-weight-bold" type="submit">
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


</body>
</html>

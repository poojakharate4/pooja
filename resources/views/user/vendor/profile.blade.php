@extends('user.layouts.master')

@section('title', 'Dashboard')

@section('content')
    <div class="container" style="margin-left: 200px;">
        <h3>Profile</h3>
        <form action="{{ route('website.vendor-profile.store') }}"
              method="post" enctype="multipart/form-data" file="true">
            @csrf
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">First Name</label>
                                        <input class="form-control form-control-sm" type="text" name="first_name"
                                               value="{{ old('first_name', $user->first_name) }}"
                                               placeholder="Enter your First Name" required>
                                        @foreach($errors->get('first_name') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Last Name</label>
                                        <input class="form-control form-control-sm" type="text" name="last_name"
                                               value="{{ old('last_name', $user->last_name) }}"
                                               placeholder="Enter your Last Name" required>
                                        @foreach($errors->get('last_name') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Email</label>
                                        <input class="form-control form-control-sm" type="email" name="email"
                                               value="{{ old('email', $user->email) }}" placeholder="Enter your email"
                                               required
                                               disabled>
                                        @foreach($errors->get('email') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Store Name</label>
                                        <input class="form-control form-control-sm" type="text" name="store_name"
                                               value="{{ old('store_name', $user->store_name) }}" placeholder="Enter your Store Name" required>
                                        @foreach($errors->get('store_name') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Store Address</label>
                                        <input class="form-control form-control-sm" type="text" name="store_address"
                                               value="{{ old('store_address', $user->address) }}" placeholder="Enter your Store Address" required>
                                        @foreach($errors->get('store_address') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Store Description</label>
                                        <input class="form-control form-control-sm" type="text" name="store_description"
                                               value="{{ old('store_description', $user->store_description) }}"  placeholder="Enter your Store Description" required>
                                        @foreach($errors->get('store_description') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Contact Number</label>
                                        <input class="form-control form-control-sm" type="text" name="contact_number"
                                               value="{{ old('contact_number', $user->contact_number) }}"  placeholder="Enter your Contact Number" required>
                                        @foreach($errors->get('contact_number') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Banner Image</label>
                                        <input type="file" name="banner_image"
                                               required accept="image/*">
                                        <img style="width:100px;height:auto;" src="{{asset(public_path('uploads/' .$user->banner_image))}}"/>
                                        @foreach($errors->get('banner_image') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-4">
                                        <label class="required">Profile Image</label>
                                        <input type="file" name="profile_image"
                                               required accept="image/*">
                                        <img style="width:100px;height:auto;" src="{{asset(public_path('uploads/' .$user->profile_image))}}"/>
                                        @foreach($errors->get('profile_image') as $error)
                                            <div class="text-danger font-weight-bold">{{ $error }}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="btn text-center justify-content-center">
                                    <button class="btn btn-success font-weight-bold" type="submit">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

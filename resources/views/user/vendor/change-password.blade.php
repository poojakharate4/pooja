@extends('user.layouts.master')

@section('title', 'Dashboard')

@section('content')
    <div class="container" style="margin-left: 200px;">
        <form action="{{ route('website.change-password.store') }}"
              method="post">
            @csrf
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="form-group mb-4">
                                <label class="required">Password</label>
                                <input class="form-control form-control-sm" type="password" name="password"
                                       placeholder="Enter your Password" required>
                                @foreach($errors->get('password') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>

                            <div class="form-group mb-4">
                                <label class="required">Confirm Password</label>
                                <input class="form-control form-control-sm" type="password" name="password_confirmation"
                                       placeholder="Enter your Confirm Password" required>
                                @foreach($errors->get('password_confirmation') as $error)
                                    <div class="text-danger font-weight-bold">{{ $error }}</div>
                                @endforeach
                            </div>
                            <div class="btn text-center justify-content-center">
                                <button class="btn btn-success font-weight-bold" type="submit">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

<nav class="navbar navbar-expand-sm bg-light" style="margin-left: 800px">
    <ul class="navbar-nav">
        <li class="nav-item">

        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Setting
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                @if(Auth::user()->roles->first()->name == 'customer')
                    <a class="nav-link" href="{{ route('website.profile') }}">Edit Profile</a>
                @else
                    <a class="nav-link" href="{{ route('website.vendor-profile') }}">Edit Profile</a>
                @endif
                <a class="nav-link" href="{{ route('website.change-password') }}">Change Password</a>
                <a class="nav-link" href="{{ route('website.login.destroy') }}">Logout</a>
            </ul>
        </li>
    </ul>
</nav>

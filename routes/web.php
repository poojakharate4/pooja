<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//
//Route::get('/', function () {
//    return view('welcome');
//});
Route::group([
    'namespace' => 'Website',
    'as' => 'website.',
], function () {
    Route::get('/redirect', 'FacebookController@redirect')->name('redirect');
    Route::get('/callback', 'FacebookController@callback')->name('callback');
    Route::get('/', 'LoginController@create')->name('login');
    Route::post('login', 'LoginController@store')->name('login.store');
    Route::get('customer/register', 'CustomerRegisterController@create')->name('customer.register');
    Route::post('customer/register/store', 'CustomerRegisterController@store')->name('customer.register.store');
    Route::get('vendor/register', 'VendorRegisterController@create')->name('vendor.register');
    Route::post('vendor/register/store', 'VendorRegisterController@store')->name('vendor.register.store');

    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('logout', 'LoginController@destroy')->name('login.destroy');
        Route::any('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('profile', 'ProfileController@create')->name('profile');
        Route::post('profile/store', 'ProfileController@store')->name('profile.store');
        Route::get('change-password', 'ChangePasswordController@create')->name('change-password');
        Route::post('change-password/store', 'ChangePasswordController@store')->name('change-password.store');
        Route::get('vendor-profile', 'VendorProfileController@create')->name('vendor-profile');
        Route::post('vendor-profile/store', 'VendorProfileController@store')->name('vendor-profile.store');
    });
});

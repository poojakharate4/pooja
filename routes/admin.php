<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//
//Route::get('/', function () {
//    return view('welcome');
//});
Route::group([
    'namespace' => 'Admin',
    'as' => 'admin.',
    'prefix' => 'admin'
], function () {
    Route::get('/', 'LoginController@create')->name('auth.login')->middleware('guest');
    Route::post('login', 'LoginController@store')->name('auth.login.store')->middleware('guest');
    Route::get('forget-password', 'ForgetPasswordController@create')->name('forget-password');
    Route::post('forget-password', 'ForgetPasswordController@store')->name('forget-password.store');
    Route::group([
        'middleware' => ['adminAuth']
    ], function () {
        Route::get('logout', 'LoginController@destroy')->name('login.destroy');
        Route::any('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('profile', 'ProfileController@create')->name('profile');
        Route::post('profile/store', 'ProfileController@store')->name('profile.store');
        Route::get('change-password', 'ChangePasswordController@create')->name('change-password');
        Route::post('change-password/store', 'ChangePasswordController@store')->name('change-password.store');

        Route::get('customer/index', 'CustomerController@index')->name('customer.index');
        Route::get('customer/create', 'CustomerController@create')->name('customer.create');
        Route::post('customer/store', 'CustomerController@store')->name('customer.store');
        Route::get('customer/{user}/edit', 'CustomerController@edit')->name('customer.edit');
        Route::post('customer/{user}/update', 'CustomerController@update')->name('customer.update');
        Route::get('customer/{user}/delete', 'CustomerController@delete')->name('customer.delete');

        Route::get('vendor/index', 'VendorController@index')->name('vendor.index');
        Route::get('vendor/create', 'VendorController@create')->name('vendor.create');
        Route::post('vendor/store', 'VendorController@store')->name('vendor.store');
        Route::get('vendor/{user}/edit', 'VendorController@edit')->name('vendor.edit');
        Route::post('vendor/{user}/update', 'VendorController@update')->name('vendor.update');
        Route::get('vendor/{user}/delete', 'VendorController@delete')->name('vendor.delete');

    });
});

<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);

        $userAdmin = User::create([
            'first_name' => 'Admin',
            'email' => 'admin@alkurn.com',
            'password' => Hash::make('alkurn@1234')
        ]);
        $userAdmin->assignRole('admin');
    }
}
